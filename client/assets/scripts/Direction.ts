const { ccclass, property } = cc._decorator;

@ccclass
export default class ACTION_STATE{
  public state: number = ACTION_STATE.STOP;
  public static readonly STOP = 0;
  public static readonly MOVE = 1;

}