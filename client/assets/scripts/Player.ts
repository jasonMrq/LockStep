import ACTION_STATE from "./Direction";
import Color = cc.Color;

const { ccclass, property } = cc._decorator;

@ccclass
export default class Player extends cc.Component {
  @property(cc.Label) private playerNameLabel: cc.Label = null;

  @property(cc.Label) private playerPos: cc.Label = null;

  @property(cc.Node) private dirFlag: cc.Node = null;

  public angle: number = 0;
  public state: number = ACTION_STATE.STOP;
  private zeroPosition: cc.Vec2 = null;
  public static readonly DISTANCE = 200;
  public bSelf:boolean = false;
  public inSkillActive:number = -1;//-1：无触发 0：开始触发  1: 正在播放技能
  public isFlashing = false;
  public x;y;

  public start() {
    this.zeroPosition = this.node.getPosition();
    this.init();
  }

  public move(dt: number) {
    if (this.state == ACTION_STATE.STOP) {
      return;
    }
    this.y +=  dt*Player.DISTANCE * (Math.sin(this.angle));
    this.x +=  dt*Player.DISTANCE * (Math.cos(this.angle));
    this.dirFlag.angle = this.angle / 3.1415926 * 180 - 90;
    this.dirFlag.x = 40 * (Math.cos(this.angle));
    this.dirFlag.y = 40 * (Math.sin(this.angle));
    this.playerPos.string = cc.js.formatStr("(%d,%d)", Math.floor(this.node.x), Math.floor(this.node.y));
  }

  public setPlayerName(name: string, isEnemy: boolean) {
    this.playerNameLabel.string = name;
    if (isEnemy) {
      this.playerNameLabel.node.color = cc.Color.RED;
      this.bSelf = false;
    } else {
      this.playerNameLabel.node.color = cc.Color.GREEN;
      this.bSelf = true;
    }
  }

  public getPlayerName(): string {
    return this.playerNameLabel.string;
  }

  public init() {
    this.angle = 0;
    this.x = this.zeroPosition.x;
    this.y = this.zeroPosition.y;
    this.node.setPosition(this.zeroPosition);
  }

  onSkill(inSkillActive:number){
    if(this.isFlashing || inSkillActive!=0){
      return;
    }
    this.isFlashing = true;
    this.node.color = Color.RED;
    let scaleTo = cc.scaleTo(1, 2);
    let scaleBack = cc.scaleTo(1, 1);
    let action = cc.sequence(scaleTo, scaleBack,cc.callFunc(()=>{
      this.inSkillActive = -1;
      this.isFlashing = false;
      this.node.setScale(1);
      this.node.color = Color.WHITE;
    }));
    this.node.runAction(action);
  }
}
