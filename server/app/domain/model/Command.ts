export default class Command {
  // 用户名
  playerName?: string;
  // 状态
  playerState: number;
  //技能激活
  skillActive: number;
  // 第几帧
  stepTime: number;
  //角度|弧度
  angle:number;
  //随机种子
  randomSeed?:number;

}