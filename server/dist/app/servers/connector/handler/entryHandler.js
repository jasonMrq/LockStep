"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app) {
    return new Handler(app);
}
exports.default = default_1;
class Handler {
    constructor(app) {
        this.app = null;
        this.sessionService = null;
        this.app = app;
        this.sessionService = app.get("sessionService");
    }
    /**
     * 准备完毕
     * @param msg
     * @param {} session
     * @returns {Promise<void>}
     */
    ready(msg, session) {
        return __awaiter(this, void 0, void 0, function* () {
            // 如果已经登录，移除之前的事件监听
            let uid = msg.uid;
            if (!!this.sessionService.getByUid(uid)) {
                session.removeAllListeners();
            }
            // 监听事件
            yield session.abind(uid);
            session.on("closed", Handler.onClosed.bind(null, this.app));
            // 加入房间
            let channelName = yield this.app.rpc.game.gameRemoter.add(session, uid, this.app.getServerId());
            session.set("rid", channelName);
            yield session.apush("rid");
            yield this.app.rpc.game.gameRemoter.start(session, channelName);
        });
    }
    /**
     * 断开连接的监听
     * @param {} app
     * @param {} session
     * @returns {Promise<void>}
     */
    static onClosed(app, session) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!session || !session.uid) {
                return;
            }
            yield app.rpc.game.gameRemoter.leave(session, session.uid, app.getServerId(), session.get("rid"));
        });
    }
}
exports.Handler = Handler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50cnlIYW5kbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vYXBwL3NlcnZlcnMvY29ubmVjdG9yL2hhbmRsZXIvZW50cnlIYW5kbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFFQSxtQkFBeUIsR0FBZ0I7SUFDckMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUM1QixDQUFDO0FBRkQsNEJBRUM7QUFFRDtJQUlJLFlBQW1CLEdBQWdCO1FBSDNCLFFBQUcsR0FBZ0IsSUFBSSxDQUFDO1FBQ3hCLG1CQUFjLEdBQW1CLElBQUksQ0FBQztRQUcxQyxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNmLElBQUksQ0FBQyxjQUFjLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRDs7Ozs7T0FLRztJQUNVLEtBQUssQ0FBQyxHQUFRLEVBQUUsT0FBd0I7O1lBQ2pELG1CQUFtQjtZQUNuQixJQUFJLEdBQUcsR0FBVyxHQUFHLENBQUMsR0FBRyxDQUFDO1lBQzFCLElBQUksQ0FBQyxDQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUN0QyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsQ0FBQzthQUNoQztZQUNELE9BQU87WUFDUCxNQUFNLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDekIsT0FBTyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzVELE9BQU87WUFDUCxJQUFJLFdBQVcsR0FBVyxNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQ3hHLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBQ2hDLE1BQU0sT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzQixNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQztRQUNwRSxDQUFDO0tBQUE7SUFFRDs7Ozs7T0FLRztJQUNLLE1BQU0sQ0FBTyxRQUFRLENBQUMsR0FBZ0IsRUFBRSxPQUF3Qjs7WUFDcEUsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUU7Z0JBQzFCLE9BQU87YUFDVjtZQUNELE1BQU0sR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsV0FBVyxFQUFFLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ3RHLENBQUM7S0FBQTtDQUNKO0FBM0NELDBCQTJDQyJ9