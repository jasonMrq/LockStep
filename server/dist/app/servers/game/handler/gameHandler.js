"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Updater_1 = require("../../../domain/Updater");
const consts_1 = require("../../../util/consts");
function default_1(app) {
    return new Handler(app);
}
exports.default = default_1;
class Handler {
    constructor(app) {
        this.app = null;
        this.app = app;
    }
    message(msg, session) {
        return __awaiter(this, void 0, void 0, function* () {
            let command = msg.command;
            command.playerName = session.uid;
            Updater_1.default.addCommand(session.get("rid"), command);
        });
    }
    getHistoryCommands(msg, session) {
        return __awaiter(this, void 0, void 0, function* () {
            let historyCommands = Updater_1.default.getHistoryCommands(session.get("rid"));
            return { code: consts_1.default.code.OK, historyCommands: historyCommands };
        });
    }
}
exports.Handler = Handler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZUhhbmRsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9hcHAvc2VydmVycy9nYW1lL2hhbmRsZXIvZ2FtZUhhbmRsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUNBLHFEQUE4QztBQUU5QyxpREFBMEM7QUFFMUMsbUJBQXlCLEdBQWdCO0lBQ3JDLE9BQU8sSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDNUIsQ0FBQztBQUZELDRCQUVDO0FBRUQ7SUFHSSxZQUFtQixHQUFnQjtRQUYzQixRQUFHLEdBQWdCLElBQUksQ0FBQztRQUc1QixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztJQUNuQixDQUFDO0lBRVksT0FBTyxDQUFDLEdBQVEsRUFBRSxPQUF1Qjs7WUFDbEQsSUFBSSxPQUFPLEdBQVksR0FBRyxDQUFDLE9BQU8sQ0FBQztZQUNuQyxPQUFPLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUM7WUFDakMsaUJBQU8sQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNwRCxDQUFDO0tBQUE7SUFFWSxrQkFBa0IsQ0FBQyxHQUFRLEVBQUUsT0FBdUI7O1lBQzdELElBQUksZUFBZSxHQUFnQixpQkFBTyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNsRixPQUFPLEVBQUUsSUFBSSxFQUFFLGdCQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxlQUFlLEVBQUUsZUFBZSxFQUFFLENBQUM7UUFDdEUsQ0FBQztLQUFBO0NBQ0o7QUFqQkQsMEJBaUJDIn0=