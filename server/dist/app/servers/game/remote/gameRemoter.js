"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../../../util/util");
const consts_1 = require("../../../util/consts");
const Updater_1 = require("../../../domain/Updater");
function default_1(app) {
    return new Remoter(app);
}
exports.default = default_1;
class Remoter {
    constructor(app) {
        this.app = null;
        this.channelService = null;
        // 正在等待的channelname
        this.waitingChannelName = null;
        this.app = app;
        this.channelService = app.get("channelService");
    }
    /**
     * 加入游戏
     * @param {string} uid
     * @param {string} sid
     * @returns {Promise<string>}
     */
    add(uid, sid) {
        return __awaiter(this, void 0, void 0, function* () {
            let channel = this.getChannel();
            channel.add(uid, sid);
            // 人数够不够开始游戏
            let uids = channel.getMembers();
            if (uids.length >= consts_1.default.ROOM_PLAYER_COUNT) {
                this.waitingChannelName = null;
            }
            return channel.name;
        });
    }
    /**
     * 离开游戏
     * @param {string} uid
     * @param {string} sid
     * @param {string} rid
     * @returns {Promise<void>}
     */
    leave(uid, sid, rid) {
        return __awaiter(this, void 0, void 0, function* () {
            let channel = this.channelService.getChannel(rid, false);
            if (!channel)
                return;
            if (channel.name === this.waitingChannelName) {
                this.waitingChannelName = null;
            }
            else {
                yield channel.apushMessage("onPlayerExit", {});
            }
            channel.destroy();
            Updater_1.default.removeRoom(channel);
            return;
        });
    }
    /**
     * 开始游戏
     * @param {string} rid
     * @returns {Promise<void>}
     */
    start(rid) {
        return __awaiter(this, void 0, void 0, function* () {
            let channel = this.channelService.getChannel(rid, false);
            let uids = channel.getMembers();
            if (uids.length >= consts_1.default.ROOM_PLAYER_COUNT) {
                // 随机排序
                uids.sort((a, b) => {
                    return Math.random() > 0.5 ? 1 : -1;
                });
                yield channel.apushMessage("onStart", {
                    playerList: uids,
                    stepInterval: consts_1.default.STEP_INTERVAL
                });
                Updater_1.default.addRoom(channel);
            }
            return;
        });
    }
    /**
     * 获取可用的channel
     * @returns {}
     */
    getChannel() {
        let channel;
        // 有等待中的channel
        if (this.waitingChannelName) {
            channel = this.channelService.getChannel(this.waitingChannelName, false);
        }
        else {
            // 生成channel name
            let name = util_1.default.makeId(8);
            while (this.channelService.getChannel(name, false)) {
                name = util_1.default.makeId(8);
            }
            // 创建channel
            channel = this.channelService.getChannel(name, true);
            this.waitingChannelName = name;
        }
        return channel;
    }
}
exports.Remoter = Remoter;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZVJlbW90ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9hcHAvc2VydmVycy9nYW1lL3JlbW90ZS9nYW1lUmVtb3Rlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQ0EsNkNBQXNDO0FBQ3RDLGlEQUEwQztBQUMxQyxxREFBOEM7QUFFOUMsbUJBQXlCLEdBQWdCO0lBQ3JDLE9BQU8sSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDNUIsQ0FBQztBQUZELDRCQUVDO0FBWUQ7SUFNSSxZQUFtQixHQUFnQjtRQUwzQixRQUFHLEdBQWdCLElBQUksQ0FBQztRQUN4QixtQkFBYyxHQUFtQixJQUFJLENBQUM7UUFDOUMsbUJBQW1CO1FBQ1gsdUJBQWtCLEdBQVcsSUFBSSxDQUFDO1FBR3RDLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2YsSUFBSSxDQUFDLGNBQWMsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ1UsR0FBRyxDQUFDLEdBQVcsRUFBRSxHQUFXOztZQUNyQyxJQUFJLE9BQU8sR0FBWSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDdEIsWUFBWTtZQUNaLElBQUksSUFBSSxHQUFhLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUMxQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksZ0JBQU0sQ0FBQyxpQkFBaUIsRUFBRTtnQkFDekMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQzthQUNsQztZQUVELE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQztRQUN4QixDQUFDO0tBQUE7SUFFRDs7Ozs7O09BTUc7SUFDVSxLQUFLLENBQUMsR0FBVyxFQUFFLEdBQVcsRUFBRSxHQUFXOztZQUNwRCxJQUFJLE9BQU8sR0FBWSxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDbEUsSUFBSSxDQUFDLE9BQU87Z0JBQUUsT0FBTztZQUVyQixJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLGtCQUFrQixFQUFFO2dCQUMxQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO2FBQ2xDO2lCQUFNO2dCQUNILE1BQU0sT0FBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDbEQ7WUFDRCxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbEIsaUJBQU8sQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFNUIsT0FBTztRQUNYLENBQUM7S0FBQTtJQUVEOzs7O09BSUc7SUFDVSxLQUFLLENBQUMsR0FBVzs7WUFDMUIsSUFBSSxPQUFPLEdBQVksSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ2xFLElBQUksSUFBSSxHQUFhLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUMxQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksZ0JBQU0sQ0FBQyxpQkFBaUIsRUFBRTtnQkFDekMsT0FBTztnQkFDUCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBUyxFQUFFLENBQVMsRUFBRSxFQUFFO29CQUMvQixPQUFPLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQ3ZDLENBQUMsQ0FBQyxDQUFDO2dCQUNILE1BQU0sT0FBTyxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUU7b0JBQ2xDLFVBQVUsRUFBRSxJQUFJO29CQUNoQixZQUFZLEVBQUUsZ0JBQU0sQ0FBQyxhQUFhO2lCQUNyQyxDQUFDLENBQUM7Z0JBQ0gsaUJBQU8sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDNUI7WUFDRCxPQUFPO1FBQ1gsQ0FBQztLQUFBO0lBRUQ7OztPQUdHO0lBQ0ssVUFBVTtRQUNkLElBQUksT0FBZ0IsQ0FBQztRQUNyQixlQUFlO1FBQ2YsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDekIsT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUM1RTthQUFNO1lBQ0gsaUJBQWlCO1lBQ2pCLElBQUksSUFBSSxHQUFXLGNBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ2hELElBQUksR0FBRyxjQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3pCO1lBQ0QsWUFBWTtZQUNaLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztTQUNsQztRQUVELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7Q0FDSjtBQS9GRCwwQkErRkMifQ==