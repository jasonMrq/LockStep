"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Room_1 = require("./model/Room");
const consts_1 = require("../util/consts");
class Updater {
    constructor() {
        this.randomSeed = 0;
    }
    /**
     * 添加房间
     * @param {} channel
     */
    static addRoom(channel) {
        let room = new Room_1.default(channel);
        this.rooms.set(channel.name, room);
    }
    /**
     * 移除房间
     * @param {} channel
     */
    static removeRoom(channel) {
        this.rooms.delete(channel.name);
    }
    /**
     * 添加命令
     * @param {string} channelName
     * @param command
     */
    static addCommand(channelName, command) {
        let room = this.rooms.get(channelName);
        room.commands.push(command);
    }
    /**
     * 获取历史命令
     * @param {string} channelName
     * @returns {Command[][]}
     */
    static getHistoryCommands(channelName) {
        return this.rooms.get(channelName).historyCommands;
    }
    /**
     * 初始化
     */
    static init() {
        this.lateUpdate = Date.now();
        setInterval(() => {
            let now = Date.now();
            let dt = now - this.lateUpdate;
            this.lateUpdate = now;
            this.update(dt);
        }, 0);
    }
    static update(dt) {
        if (this.rooms.size <= 0)
            return;
        // 遍历房间来更新帧
        this.rooms.forEach((room) => {
            // 大于一帧的间隔
            room.stepUpdateTime += dt;
            if (room.stepUpdateTime >= consts_1.default.STEP_INTERVAL) {
                room.stepUpdateTime -= consts_1.default.STEP_INTERVAL;
                room.stepTime++;
                this.stepUpdate(room);
            }
        });
    }
    /**
     * 更新一帧
     * @param {Room} room
     */
    static stepUpdate(room) {
        // 过滤指令
        let uids = room.channel.getMembers();
        let commands = [];
        for (let uid of uids) {
            commands.push({ playerName: uid, stepTime: room.stepTime, angle: 0, playerState: 0, skillActive: -1 });
        }
        for (let roomCommand of room.commands) {
            for (let command of commands) {
                if (roomCommand.playerName === command.playerName) {
                    command.angle = roomCommand.angle;
                    command.playerState = roomCommand.playerState;
                    command.skillActive = roomCommand.skillActive;
                }
            }
        }
        // 记录到历史指令（用于重连）
        room.historyCommands.push(commands);
        room.commands = [];
        // 发帧
        room.channel.apushMessage("onMessage", { commands: commands });
    }
}
// 分割房间
Updater.rooms = new Map();
// 上次更新时间（用来控制update更新）
Updater.lateUpdate = 0;
exports.default = Updater;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXBkYXRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2FwcC9kb21haW4vVXBkYXRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVDQUFnQztBQUNoQywyQ0FBb0M7QUFHcEM7SUFBQTtRQUtZLGVBQVUsR0FBVyxDQUFDLENBQUM7SUEyRm5DLENBQUM7SUF6Rkc7OztPQUdHO0lBQ0ksTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFnQjtRQUNsQyxJQUFJLElBQUksR0FBUyxJQUFJLGNBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRDs7O09BR0c7SUFDSSxNQUFNLENBQUMsVUFBVSxDQUFDLE9BQWdCO1FBQ3JDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxVQUFVLENBQUMsV0FBbUIsRUFBRSxPQUFnQjtRQUMxRCxJQUFJLElBQUksR0FBUyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxXQUFtQjtRQUNoRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLGVBQWUsQ0FBQztJQUN2RCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxNQUFNLENBQUMsSUFBSTtRQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQzdCLFdBQVcsQ0FBQyxHQUFHLEVBQUU7WUFDYixJQUFJLEdBQUcsR0FBVyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDN0IsSUFBSSxFQUFFLEdBQVcsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDdkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUM7WUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNwQixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDVixDQUFDO0lBRU8sTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFVO1FBQzVCLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQztZQUFFLE9BQU87UUFDakMsV0FBVztRQUNYLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBVSxFQUFFLEVBQUU7WUFDOUIsVUFBVTtZQUNWLElBQUksQ0FBQyxjQUFjLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxnQkFBTSxDQUFDLGFBQWEsRUFBRTtnQkFDN0MsSUFBSSxDQUFDLGNBQWMsSUFBSSxnQkFBTSxDQUFDLGFBQWEsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3pCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ssTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFVO1FBQ2hDLE9BQU87UUFDUCxJQUFJLElBQUksR0FBYSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQy9DLElBQUksUUFBUSxHQUFjLEVBQUUsQ0FBQztRQUM3QixLQUFLLElBQUksR0FBRyxJQUFJLElBQUksRUFBRTtZQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUMsVUFBVSxFQUFDLEdBQUcsRUFBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBQyxLQUFLLEVBQUMsQ0FBQyxFQUFDLFdBQVcsRUFBQyxDQUFDLEVBQUMsV0FBVyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQztTQUNoRztRQUNELEtBQUssSUFBSSxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNuQyxLQUFLLElBQUksT0FBTyxJQUFJLFFBQVEsRUFBRTtnQkFDMUIsSUFBSSxXQUFXLENBQUMsVUFBVSxLQUFLLE9BQU8sQ0FBQyxVQUFVLEVBQUU7b0JBQy9DLE9BQU8sQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQztvQkFDbEMsT0FBTyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO29CQUM5QyxPQUFPLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUM7aUJBQ2pEO2FBQ0o7U0FDSjtRQUNELGdCQUFnQjtRQUNoQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNuQixLQUFLO1FBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDbkUsQ0FBQzs7QUE5RkQsT0FBTztBQUNRLGFBQUssR0FBc0IsSUFBSSxHQUFHLEVBQWdCLENBQUM7QUFDbEUsdUJBQXVCO0FBQ1Isa0JBQVUsR0FBVyxDQUFDLENBQUM7QUFKMUMsMEJBZ0dDIn0=