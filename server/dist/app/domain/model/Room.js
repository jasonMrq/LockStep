"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Room {
    constructor(channel) {
        this.channel = null;
        // 第几帧
        this.stepTime = 0;
        // 这帧执行了多久
        this.stepUpdateTime = 0;
        // 当前命令
        this.commands = [];
        // 历史命令
        this.historyCommands = [];
        this.channel = channel;
    }
}
exports.default = Room;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUm9vbS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2FwcC9kb21haW4vbW9kZWwvUm9vbS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBO0lBV0ksWUFBbUIsT0FBZ0I7UUFWNUIsWUFBTyxHQUFZLElBQUksQ0FBQztRQUMvQixNQUFNO1FBQ0MsYUFBUSxHQUFXLENBQUMsQ0FBQztRQUM1QixVQUFVO1FBQ0gsbUJBQWMsR0FBVyxDQUFDLENBQUM7UUFDbEMsT0FBTztRQUNBLGFBQVEsR0FBYyxFQUFFLENBQUM7UUFDaEMsT0FBTztRQUNBLG9CQUFlLEdBQWdCLEVBQUUsQ0FBQztRQUdyQyxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztJQUMzQixDQUFDO0NBQ0o7QUFkRCx1QkFjQyJ9